class Address < ApplicationRecord
  # Association
  has_many :customers

  # Validation
  validates :street, :city, :zip_code, presence: true
end
