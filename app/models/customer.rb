class Customer < ApplicationRecord
  # Association
  belongs_to :address

  # Validation
  validates :name , :address, presence: true
  validates_associated :address

  def self.filter_by_name_or_street(filter_string)
    results = self.includes(:address).references(:addresses)
    results = results.where('name ILIKE ? OR addresses.street ILIKE ?', "%#{filter_string}%", "%#{filter_string}%") unless filter_string.blank? 
    results
  end

  def self.new_with_build_address(customer_params, address_params)
    c = Customer.new(customer_params)
    c.build_address(address_params)
    c
  end

end
