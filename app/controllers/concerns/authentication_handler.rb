module AuthenticationHandler

  class AuthenticationError < StandardError; end

  def authenticate_request
    raise AuthenticationError unless authorize?
  end

  def request_headers
    request.headers
  end
  
  def authorization_token
    'AuthorizationToken'
  end  

  def header_authorization
    request_headers['Authorization'].to_s.split(' ').last 
  end

  def authorize?
     header_authorization.eql? authorization_token
  end  

end