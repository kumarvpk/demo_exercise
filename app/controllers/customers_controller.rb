class CustomersController < ApplicationController
  before_action :set_customer, only: [:destroy]

  # GET /customers
  def index
    @customers = Customer.filter_by_name_or_street(params[:search]).all
    json_response(@customers)
  end

  # POST /customers
  def create
    @customer = Customer.new_with_build_address(customer_params, address_params)

    if @customer.save
      json_response(@customer, :created, @customer)
    else
      json_response(@customer.errors, :unprocessable_entity)
    end
  end

  # DELETE /customers/1
  def destroy
    @customer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def address_params
      params.require(:address).permit(:street, :city, :zip_code)
    end

    def customer_params
      params.require(:customer).permit(:name)
    end
end
