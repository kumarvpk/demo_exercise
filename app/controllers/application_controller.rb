class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler
  include AuthenticationHandler

  before_action :authenticate_request

end
