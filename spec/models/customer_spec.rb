require 'rails_helper'

RSpec.describe Customer, type: :model do  
  context 'associations' do
    it { should belong_to(:address) }
  end

  context 'validations' do
    context 'name should be present' do
      let(:valid_object) { build(:customer) }
      let(:invalid_object) { build(:customer, :invalid_name) }
      it { expect(valid_object.name).to be_present }
      it { expect(invalid_object.name).not_to be_present }
    end
  end  
end
