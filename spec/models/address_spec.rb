require 'rails_helper'

RSpec.describe Address, type: :model do

  context 'associations' do
    it { should have_many(:customers)}
  end

  context 'validations' do
    context 'street should be present' do
      let(:valid_object) { build(:address) }
      let(:invalid_object) { build(:address, :invalid_street) }
      it { expect(valid_object.street).to be_present }
      it { expect(invalid_object.street).not_to be_present }
    end

    context 'city should be present' do
      let(:valid_object) { build(:address) }
      let(:invalid_object) { build(:address, :invalid_city) }
      it { expect(valid_object.city).to be_present }
      it { expect(invalid_object.city).not_to be_present }
    end

    context 'zip_code should be present' do
      let(:valid_object) { build(:address) }
      let(:invalid_object) { build(:address, :invalid_zip_code) }
      it { expect(valid_object.zip_code).to be_present }
      it { expect(invalid_object.zip_code).not_to be_present }
    end
  end
    
end
