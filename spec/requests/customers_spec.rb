require 'rails_helper'

RSpec.describe "Customers", type: :request do

  let(:authorization_header) { {'Authorization' => 'AuthorizationToken'} }

  describe "GET /customers" do
    it "customers list" do
      address = Address.create! attributes_for(:address)
      customer1 = Customer.create! attributes_for(:customer).merge({address_id: address.id})      
      customer2 = Customer.create! attributes_for(:customer).merge({address_id: address.id})
      get customers_path, headers: authorization_header
      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(200)
      expect(json.count).to eq(2)
      expect(json.collect{|j| j['name'] }).to include(customer1.name)
      expect(json.collect{|j| j['name'] }).to include(customer2.name)
    end

    it "customers list with filter" do
      address = Address.create! attributes_for(:address)
      customer1 = Customer.create! attributes_for(:customer).merge({address_id: address.id})      
      customer2 = Customer.create! attributes_for(:customer).merge({address_id: address.id})
      get customers_path, { params: {search: customer1.name}, headers: authorization_header }
      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(200)
      expect(json.count).to eq(1)
      expect(json.collect{|j| j['name'] }).to include(customer1.name)
      expect(json.collect{|j| j['name'] }).to_not include(customer2.name)
    end

    it "return 401 without authorization" do
      address = Address.create! attributes_for(:address)
      customer1 = Customer.create! attributes_for(:customer).merge({address_id: address.id})      
      customer2 = Customer.create! attributes_for(:customer).merge({address_id: address.id})
      get customers_path
      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('You are not authorized')
    end

  end

  describe "POST /customers" do
    it "create customer" do
      post customers_path, {params: {customer: attributes_for(:customer), address: attributes_for(:address)} , headers: authorization_header }
      expect(response).to have_http_status(201)
      expect(Customer.count).to eq(1)
    end

    it "return 401 without authorization" do
      post customers_path, {params: {customer: attributes_for(:customer), address: attributes_for(:address)} }
      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('You are not authorized')
    end
  end

  describe "DELETE /customers" do
    it "delete customer" do
      address = Address.create! attributes_for(:address)
      customer1 = Customer.create! attributes_for(:customer).merge({address_id: address.id})      
      customer2 = Customer.create! attributes_for(:customer).merge({address_id: address.id})      

      delete customer_path(customer1.id), headers: authorization_header 
      expect(response).to have_http_status(204)
      expect(Customer.count).to eq(1)
      expect(Customer.all.collect{|j| j.name }).to include(customer2.name)
      expect(Customer.all.collect{|j| j.name }).to_not include(customer1.name)
    end

    it "return 401 without authorization" do
      address = Address.create! attributes_for(:address)
      customer1 = Customer.create! attributes_for(:customer).merge({address_id: address.id})      
      customer2 = Customer.create! attributes_for(:customer).merge({address_id: address.id})      

      delete customer_path(customer1.id) 
      expect(response.content_type).to eq('application/json')
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('You are not authorized')
    end

  end

end
