FactoryGirl.define do
  factory :customer do
    association :address

    name { Faker::FunnyName.name }

    trait :invalid_name do
      name { nil }
    end
  end
end