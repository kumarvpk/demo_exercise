FactoryGirl.define do
  factory :address do

    street { Faker::Address.street_name }
    city { Faker::Address.city }
    zip_code { Faker::Address.zip_code }

    trait :invalid_street do
      street { nil }
    end

    trait :invalid_city do
      city { nil }
    end

    trait :invalid_zip_code do
      zip_code { nil }
    end

  end
end